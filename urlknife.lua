function prepare_path(path)
	-- split the path up into sections.
	local i,previ = 0,0
	local urltab = {}
	while i < #path do
		i = path:find("/",previ) or -1
			local chunk = path:sub(previ,i-1)
		if i == -1 then 
			chunk = path:sub(previ,-1) 
		end
			if chunk == "." then
				-- do nothing
			elseif chunk == ".." then
				--remove previous item from table.
				table.remove(urltab)
			else
				-- TODO: add more test cases!
				table.insert(urltab,chunk)
			end
		previ = i+1
		if i == -1 then break end
	end
	return table.concat(urltab,"/")
end

function choppath(path)
	if path:sub(1,1) == "/" then
		return path:sub(2,-1)
	end
	return path
end

function percent_enc(str)
-- !#$%&'()*+,/:;=?@[]
local i = 1
local strtab = {} --takes more memory, but faster than ..
while i <= #str do
	local c = str:byte(i)
	-- check to see if char is in acceptable range
	if (c > 64 and c < 90) 
	or (c >= 97 and c <= 122)
	or (c >= 48 and c <= 57)
	or c == 46 or c == 45 or c == 95 or c == 92 then
		strtab[i] = string.char(c)
	else
		strtab[i] = "%"..(string.format("%x",c))
	end
	i = i+1
end
return table.concat(strtab,"")
end

function percent_dec(str)
	return str:gsub("%%(..)",function(n) return string.char(tonumber("0x"..n)) end)
end
