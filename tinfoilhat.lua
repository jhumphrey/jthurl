return {
	["nasty_chars"] = {
		["\""] = "&quot;",
		["\'"] = "&#39;",
		["<"] = "&lt;",
		[">"] = "&gt;",
		["&"] = "&#38;"
	},
	["nc_order"] = {
		"&","\"","\'","<",">"
	},
	["io_enc"] = function(self,value)
		for i, v in ipairs(self.nc_order) do
			value = value:gsub(v, self.nasty_chars[v]) --TODO: optimize?
		end
		return value
	end
}
